global.ROOTURL = 'http://localhost:3000'
global.ROOTPATH = require('path').dirname(__dirname)
global.INCLUDESPATH = global.ROOTPATH + '/includes'
global.APPPATH = global.ROOTPATH + '/app'
global.CONFIGSPATH = global.ROOTPATH + '/configs'
global.APPROUTESPATH = global.APPPATH + '/routes'
global.APPCONTROLLERSPATH = global.APPPATH + '/controllers'
global.APPMODELSPATH = global.APPPATH + '/models'
