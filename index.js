require('./includes/init.js');

const express = require("express")
const userModel = require(APPMODELSPATH + "/userModel.js")
var app = express()
var bodyParser = require("body-parser")
var jwt = require('jsonwebtoken')
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use( function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
      jwt.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', async function(err, decode) {
        user = await userModel.findById(decode._id).exec()
        if (user && !err) {
          req.user = user;
        } else {
          req.user = undefined;
        }
        next();
      });
    } else {
      req.user = undefined;
      next();
    }
});
app.use('/',  require(global.APPROUTESPATH + '/client.js'))
app.use('/admin',  require(global.APPROUTESPATH + '/admin.js'))
var port = process.env.PORT || 3333
app.listen(port)
