const express = require('express')
const route = express.Router()

const userCtrl = require(APPCONTROLLERSPATH + '/admin/userCtrl.js')
const postCtrl = require(APPCONTROLLERSPATH + '/admin/postCtrl.js')

route.get('/posts(/bycategory/:category)?(/bytag/:tag)?(/byauthor/:author)?', postCtrl.getList)
route.get('/post/:_id', postCtrl.get)

module.exports = route