const express = require('express')
const route = express.Router()

const userCtrl = require(APPCONTROLLERSPATH + '/admin/userCtrl.js')
const postCtrl = require(APPCONTROLLERSPATH + '/admin/postCtrl.js')

route.route('/register')
    .post(userCtrl.register)
route.route('/login')
    .post(userCtrl.loginAuth)
    
// Yêu cầu đăng nhập
route.use(userCtrl.loginRequired)

route.route('/user/:username')
    .get(userCtrl.getByUsername)

route.route('/post/create')
    .post(postCtrl.create)

route.route('/post/:_id')
    .get(postCtrl.get)
    .post(postCtrl.update)

module.exports = route