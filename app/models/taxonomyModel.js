let mongoose = require('mongoose')
let taxonomySchema = new mongoose.Schema({
    name: { 
        type: String,
        required: true,
        default: 'Draft ' + Date.now()
    },
    slugName: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true,
        default: 'draft-' + Date.now()
    },
    descripton: String,
    excerpt: String,
    editing: {
        yes: Boolean,
        editBy: { type: ObjectId, ref: users }
    },
    taxnonomyType: String,
    taxnonomyMeta: [{
        name: String,
        value: Mixed
    }],
    parent: { type: ObjectId, ref: posts },
    createBy: { type: ObjectId, ref: users },
    createAt: { type: Date, default: Date.now },
    updateAt: { type: Date, default: Date.now }
}, { collection: 'taxonomys' });

module.exports = mongoose.model('taxonomys', taxonomySchema)