let mongoose = require('mongoose')
const { ObjectId, Mixed } = mongoose.Schema.Types
let postSchema = new mongoose.Schema({
    title: { 
        type: String,
        required: true,
        default: 'Draft ' + Date.now()
    },
    slugTitle: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true,
        default: 'draft-' + Date.now()
    },
    content: String,
    excerpt: { 
        type: String,
        default: function() {
            if (this.content) {
                return this.content.substr(0,20)
            }
        }
    },
    postType: {
        type: String,
        required: true,
        default: 'post'
    },
    featureImage: String,
    postTaxonomy: {
        type: [{ 
            _id: {
                type: ObjectId, 
                ref: 'taxonomys' 
            }
        }], 
        default: [] 
    },
    postMeta: [{
        name: String,
        value: Mixed
    }],
    guid: {
        type: String,
        default: function() {
            return '/post/' + this._id
        }
    },
    permalink: {
        type: String,
        default: function() {
            console.log(this)
            if (this.permalink == null) return this.guid
            else return this.permalink
        }
    },
    status: {
        type: String,
        enum: ['created', 'published', 'pending', 'movedtotrash']
    },
    editing: {
        yes: Boolean,
        editBy: { type: ObjectId, ref: 'users' }
    },
    parent: { type: ObjectId, ref: 'posts' },
    createBy: { type: ObjectId, ref: 'users' },
    createAt: { type: Date, default: Date.now },
    updateAt: { type: Date, default: Date.now },
    history: [{
        action: String,
        content: String,
        createBy: { type: ObjectId, ref: 'users' },
        createAt: { type: Date, default: Date.now },
    }]
}, { collection: 'posts' });

module.exports = mongoose.model('posts', postSchema)