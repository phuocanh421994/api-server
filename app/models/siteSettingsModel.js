let mongoose = require('mongoose')
const { ObjectId, Mixed } = mongoose.Schema.Types
let siteSettingsSchema = new mongoose.Schema({

    // General settings
    siteName: String,
    siteTitle: String,
    siteUrl: String,
    emailSupport: String,
    allowSSL: { type: Boolean, default: false },
    currencySymbol: String,
    maxResultPerPageList: { type: String, default: 50, validate: {
        validator: function(v) {
            return v > 0;
        },
        message: number => `${number.value} is not > 0 number!`
    }},
    timeZone: String,

    // Member Settings
    requireEmailVerifycation: { type: Boolean, default: false }, // force users to verify their emails to activate their accounts.
    requireEmailUpdateVerifycation: { type: Boolean, default: false }, // force users to verify their new emails when they update their profiles
    requireWithdrawWithSameDepositGateway: { type: Boolean, default: false }, //  This option is automatically disabled for members that did not make any deposit
    numberOfDaysToInactive: Number,
    allowMoneyTransfer:  { type: Boolean, default: false }, // members can tranfer money from account balance to purchase balance
    messageSystem: { type: Boolean, default: false }, // members can send messages between them
    allowCancelPendingWithdraw: { type: Boolean, default: false }, // members can cancel their pending withdrawal and get money back to thei main balance.

    // Ad click settings
    minClicksToWithdraw: { type: Number, default: 0 }, // Minimum clicks needed to cashout. 0 to disable
    requireClickToday: { type: Boolean, default: false }, // force users to make clicks today to earn from referrals tomorrow
    numberClicksToday:  { type: Number, default: 0 }, // Number of clicks neededs today to earn from referrals tomorrow
    forceView:  { type: Boolean, default: true }, // Timer will stop if user change of window
    autorunSurfbarAfter: { type: Number, default: 0 }, //Seconds to autorun surfbar if ad is slow. Set < 0 to disable,

    // Maintenance
    maintenanceMode:  { type: Boolean, default: false }, // turns site off when enabled,
    maintenanceMessage: String, // 

    // Mail settings
    emailName: String, //
    emailAddress: String, //
    mailType: {
        type: String,
        enum: ['Server mail', 'SMTP mail']
    }
}, { collection: 'siteSettings' });

module.exports = mongoose.model('siteSettings', siteSettingsSchema)