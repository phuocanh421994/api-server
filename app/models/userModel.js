let mongoose = require('mongoose')
const bcrypt = require('bcrypt')
let userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    id: String,
    email: String,
    verified_email: Boolean,
    name: String,
    given_name: String,
    family_name: String,
    link: String,
    picture: String,
    locale: String,
    tokens: {
        'access_token': String,
        'scope': String,
        'token_type': String,
        'id_token': String,
        'expiry_date': String
    },
    payment_gateways: [{
       gateway_name: String,
       gateway_address: String 
    }],
    upline: String,
    createAt: { type : Date, default: Date.now },
    updateAt: { type : Date, default: Date.now },
    lastlogin: {
        timestamp: String,
        ip: String
    },
    history: [{
        active: String,
        amount: Number,
        points: Number,
        ip: String,
        geolocation: {
            country: String,
            country_code: String,
            region: String,
            region_code: String,
            city: String
        },
        browser: String,
        status: { type: String, default: 'Success' },
        failureReason: String,
        createAt: { type : Date, default: Date.now }
    }]
}, { collection: 'users' })

userSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

userSchema.virtual('balance').get(function () {
    return history.reduce(getSumAmount, 0);
})

userSchema.virtual('points').get(function () {
    return history.reduce(getSumPonits, 0);
})

function getSumAmount(total, elem) {
        return total + elem.amount;
}

function getSumPonits(total, elem) {
        return total + elem.points;
}

userSchema.pre('findOneAndUpdate', (next) => {
    next()
})
module.exports = mongoose.model('users', userSchema)