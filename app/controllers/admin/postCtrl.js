const   postModel = require(APPMODELSPATH + "/postModel.js"),
        bcrypt = require('bcrypt'),
        jwt = require('jsonwebtoken')

const postCtrl = {
    create: async (req, res, next) => {
        var body = req.body
        try {
            const post = await postModel.create({
                title: 'Draft ' + Date.now(),
                slugTitle: 'draft-' + Date.now(),
                postType: body.type || 'post',
                status: 'created',
                createBy: req.user._id,
                history: {
                    action: 'created',
                    content: body.content,
                    createBy: req.user._id
                }
            })
            return res.json(post)
        } catch (error) {
            console.log(error)
            return res.json({err: error})
        }
    },
    update: async (req, res, next) => {
        var params = req.params
        var body = req.body
        Object.assign(body, {
            postType: body.type || 'post',
            postMeta: postCtrl.getPostMeta(),
            permalink: body.permalink || postCtrl.getPermalink(params._id),
            status: body.status || 'created',
            updateAt: Date.now(),
        })

        try {
            var post = await postModel.findOneAndUpdate({
                _id: params._id
            }, body).exec()
            
            if (body.taxonomys) {
                post = await postModel.findOneAndUpdate({
                    _id: post._id
                }, {
                    $addToSet: {
                        postTaxonomy: {
                            $each: body.taxonomys
                        }
                    }
                }).exec()
            }
            post = await postModel.findOneAndUpdate({
                _id: post._id
            }, {
                $push: {
                    history: {
                        action: 'update',
                        content: body.content,
                        createBy: req.user._id,
                        createAt: Date.now()
                    }
                }
            }).exec()

            return res.json(post)
        } catch (error) {
            console.error(error)
            //return res.json(error)
        }
    },
    get: async (req, res, next) => {
        var body = req.params
        try {
            const post = await postModel.findOne({ $or: [
                {_id: body._id},
                {slugTitle: body.slug}
            ]}).exec()
            if (post) return res.json(post)
            else return res.json({err: 'Post is not exist', params: body})
        } catch (error) {
            console.error(error)
            return res.json('' + error)
        }
    },
    getList: async (req, res, next) => {
        var {category, tag, author} = req.params
        try {
            const post = await postModel.find({
                category: category,
                tag: tag,
                author: author
            }).exec()
            if (post) return res.json(post)
            else return res.json({err: 'Post is not exist', params: body})
        } catch (error) {
            console.error(error)
            return res.json('' + error)
        }
    },
    getPermalink: function(title) {
        var permalink = title
        /**** Handle permalink ****/


        /**** End handle permalink ****/
        return permalink
    },
    getPostMeta: function() {
        var metaLists  = Array()
        /**** Handle metalists ****/


        /**** End handle metalists ****/
        return metaLists
    }
}
module.exports = postCtrl