const   userModel = require(APPMODELSPATH + "/userModel.js"),
        bcrypt = require('bcrypt'),
        jwt = require('jsonwebtoken')

const userCtrl = {
    getByUsername: async function (req, res, next) {
        const body = req.params
        const user = await userModel.findOne({
            username: body.username
        }).exec()
        if (user) {
            return res.json(user)
        } else {
            return res.json({ error: "Username is not exist"})
        }
    },
    loginAuth: async function (req, res, next) {
        const body = req.body
        const user = await userModel.findOne({
            username: body.username
        }).exec()
        if (user) {
            if ( user.comparePassword(body.password) ) return res.json({
                token: jwt.sign({ 
                    _id: user._id, 
                    lastlogin: Date.now(), 
                    lastip: req.ip 
                }, 
                'RESTFULAPIs') 
            })
            else return res.json({ error: "Password is not right"})
        } else {
            return res.json({ error: "Username is not exist"})
        }
    },
    register: async function (req, res, next) {
        const body = req.body
        const verify = await userModel.findOne({
            username: body.username
        }).exec()
        if (verify) {
            return res.json({error: "username is used"})
        }
        const user = await userModel.create({ 
            username: body.username, 
            password: bcrypt.hashSync(req.body.password, 10) 
        });

        return res.json(user)
    },
    loginRequired: function(req, res, next) {
        if (req.user) {
            next();
        } else {
            if (req.originalUrl.indexOf('/login') < 0 && req.originalUrl.indexOf('/register') < 0 ) {
                return res.status(401).json({ message: 'Unauthorized user!' });
            } else {
                next();
            }
        }
    }
}
module.exports = userCtrl